if (process.env.APM_SECRET_TOKEN) {
  require("elastic-apm-node").start({
    // Override service name from package.json
    // Allowed characters: a-z, A-Z, 0-9, -, _, and space
    serviceName: "url-shortener",

    // Use if APM Server requires a token
    secretToken: process.env.APM_SECRET_TOKEN,

    // Set custom APM Server URL (default: http://localhost:8200)
    serverUrl: "https://apm-server-apm-http.logging:8200",

    verifyServerCert: false,
    cloudProvider: "none",
  });
}

const express = require("express");
const mongoose = require("mongoose");
const ShortUrl = require("./models/shortUrl");
const app = express();
const winston = require("winston");

const logger = winston.createLogger({
  level: "info",
  format: winston.format.json(),
  defaultMeta: { service: "user-service" },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new winston.transports.File({ filename: "error.log", level: "error" }),
    new winston.transports.File({ filename: "combined.log" }),
  ],
});

if (process.env.NODE_ENV !== "production") {
  logger.add(
    new winston.transports.Console({
      format: winston.format.simple(),
    })
  );
}

mongoose.connect(`mongodb://${process.env.DB_HOST}/urlShortener`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: false }));

app.get("/health", async (req, res) => {
  logger.info("Health check called");
  res.json({ status: "UP" });
});

app.get("/", async (req, res) => {
  logger.info("Display front page");
  const shortUrls = await ShortUrl.find();

  res.render("index", { shortUrls: shortUrls });
});

app.post("/shortUrls", async (req, res) => {
  logger.info(`Creating short url for url ${req.body.fullUrl}`);
  await ShortUrl.create({ full: req.body.fullUrl });

  res.redirect("/");
});

app.post("/deleteUrls", async (req, res) => {
  logger.info("Deleting urls");
  await ShortUrl.deleteMany();

  res.redirect("/");
});

app.get("/:shortUrl", async (req, res) => {
  const shortUrl = await ShortUrl.findOne({ short: req.params.shortUrl });
  if (shortUrl == null) return res.sendStatus(404);
  logger.info(
    `Redirecting the short url "${req.params.shortUrl}" to the full url "${shortUrl.full}"`
  );

  shortUrl.clicks++;
  shortUrl.save();

  res.redirect(shortUrl.full);
});

app.listen(process.env.PORT || 8080, () => {
  logger.info("App is listening");
});
