FROM node:alpine as base

FROM base as builder

WORKDIR /dependencies

COPY ./package.json .
RUN npm install && npm install nodemon

FROM base as app

WORKDIR /app

COPY --from=builder /dependencies/node_modules /app/node_modules
COPY ./src .

CMD ["npx", "nodemon", "server.js"]
