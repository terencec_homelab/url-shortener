# urlShortener

Small URL shortener made with Node.js and Mongo

Inspired by [this video](https://youtu.be/SLpUKAGnm-g) from Web Dev Simplified.
